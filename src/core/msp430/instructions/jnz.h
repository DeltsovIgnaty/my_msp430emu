//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_JNZ_H
#define MSP430EMU_JNZ_H

#include "../../base/interfaces/AInstruction.h"
#include "../operands/Void.h"
#include "../operands/Register.h"
#include "../hardware/CPU.h"

namespace msp430 {
    class Jnz : public vmc::AInstruction {
    public:
        Jnz(const vmc::PAOperand vop1) :
                AInstruction("jnz", vop1, std::make_shared<Void>(), 2) { }

        bool execute(vmc::PDevice dev) override {
            auto z = Register::SR->bit(dev, STATUS_Z);
            if (z==0) {
                auto pc = Register::PC->value(dev);
                pc += op1->value(dev) + size;
                Register::PC->value(dev, pc);
            }
            return true;
        }
    };
}

#endif //MSP430EMU_JNZ_H
