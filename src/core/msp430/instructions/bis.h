//
// Created by Alexei Gladkikh on 05/10/16.
//

#ifndef MSP430EMU_BIS_H
#define MSP430EMU_BIS_H

#include "../../base/interfaces/AInstruction.h"

namespace msp430 {
    class Bis : public vmc::AInstruction {
    public:
        Bis(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("bis", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) override {
            auto src = this->op1->value(dev);
            auto dst = this->op2->value(dev);
            auto res = (uint32_t) (src || dst);
            this->op2->value(dev, res);
            return true;
        }
    };
}

#endif //MSP430EMU_BIS_H
