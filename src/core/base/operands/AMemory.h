//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_AMEMORY_H
#define MSP430EMU_AMEMORY_H

#include "../interfaces/AOperand.h"

namespace vmc {
    class AMemory : public AOperand {
    public:
        AMemory(Size vsize) : AOperand(MEMORY, vsize) { };
    };
}

#endif //MSP430EMU_AMEMORY_H
