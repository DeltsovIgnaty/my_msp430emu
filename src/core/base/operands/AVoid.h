//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_AVOID_H
#define MSP430EMU_AVOID_H

#include "../interfaces/AOperand.h"

namespace vmc {
    class AVoid : public AOperand {
    public:
        AVoid() : AOperand(VOID, UNDEF) { };

        std::string stringify() override {
            return std::string();
        }
    };
}

#endif //MSP430EMU_AVOID_H
