//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_AIMMEDIATE_H
#define MSP430EMU_AIMMEDIATE_H

#include "../interfaces/AOperand.h"

namespace vmc {
    class AImmediate : public AOperand {
    public:
        AImmediate(Size vsize) : AOperand(IMMEDIATE, vsize) { };
    };
}

#endif //MSP430EMU_AIMMEDIATE_H
