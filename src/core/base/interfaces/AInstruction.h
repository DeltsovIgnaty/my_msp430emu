//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_IINSTRUCTION_H
#define MSP430EMU_IINSTRUCTION_H

#include <cstdint>
#include <memory>
#include "../../../utils/utils.h"

namespace vmc {
    class ADevice;
    typedef std::shared_ptr<ADevice> PDevice;

    class AOperand;
    typedef std::shared_ptr<AOperand> PAOperand;

    class AInstruction {
    public:
        // Currently unused
        uint32_t flags;

        // Ticks count for current instruction
        const uint32_t ticks = 1;

        // Must be initialized in inherited constructor
        const std::string mnem;
        const PAOperand op1;
        const PAOperand op2;

        const int size;

        // Changed by device->execute method
        uint32_t ea;

        // Mandatory overload method:
        //   return true  - if execution has been ok and can be continued
        //          false - something went wrong
        virtual bool execute(PDevice dev) = 0;

        AInstruction(const std::string vmnem,
                     const PAOperand vop1,
                     const PAOperand vop2,
                     const int vsize)
                : mnem(vmnem), size(vsize), op1(vop1), op2(vop2) { };

        std::string stringify();
    };

    typedef std::shared_ptr<AInstruction> PAInstruction;
}

#endif //MSP430EMU_IINSTRUCTION_H
